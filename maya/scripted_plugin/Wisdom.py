import wisdom

def initializePlugin(mobject):
	wisdom.load()

def uninitializePlugin(mobject):
	wisdom.unload()
